public class MyDateTime extends Object{
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public String toString() {
        return date + " " +  time;
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        incrementHour(1);

    }

    public void incrementHour(int diff) {
        int dayDiff = time.incrementHour(diff);
        if (dayDiff <0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);

    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {

        int dayDiff = time.incrementMinute(diff);
        if (dayDiff <0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);


    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int diff) {
        date.incrementDay(diff);
    }

    public void decrementMonth(int diff) {
        date.decrementMonth(diff);
    }

    public void decrementDay(int diff) {
        date.decrementDay(diff);
    }

    public void incrementMonth(int diff) {
        date.incrementMonth(diff);
    }

    public void decrementYear(int diff) {
        date.decrementYear(diff);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        if ((date.year - anotherDateTime.date.year) > 0) {
            return false;
        } else if ((date.month - anotherDateTime.date.month) > 0) {
            return false;
        } else if ((date.day - anotherDateTime.date.day) > 0) {
            return false;
        } else if ((time.hour - anotherDateTime.time.hour) > 0) {
            return false;
        } else if ((time.minute - anotherDateTime.time.minute) > 0) {
            return false;
        } else
            return true;

    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        if ((date.year - anotherDateTime.date.year) < 0) {
            return false;
        } else if ((date.month - anotherDateTime.date.month) < 0) {
            return false;
        } else if ((date.day - anotherDateTime.date.day) < 0) {
            return false;
        } else if ((time.hour - anotherDateTime.time.hour) < 0) {
            return false;
        } else if ((time.minute - anotherDateTime.time.minute) < 0) {
            return false;
        } else
            return true;

    }


    public String dayTimeDifference(MyDateTime anotherDateTime) {
        int firstLeap = date.year / 4;
        int secondLeap = anotherDateTime.date.year / 4;
        int thMonth = 0;
        int sum = 0;
        while (thMonth < date.month-1){
            if (!date.inLeapYear() && thMonth == 1)
                date.maxDays[thMonth] = 28;
            sum += date.maxDays[thMonth];
            thMonth++;
        }
        long firstDate = (date.year * 365) + sum + firstLeap + date.day;
        thMonth = 0;
        sum = 0;
        while (thMonth < anotherDateTime.date.month-1){
            if (!anotherDateTime.date.inLeapYear() && thMonth == 1)
                anotherDateTime.date.maxDays[thMonth] = 28;
            sum += anotherDateTime.date.maxDays[thMonth];
            thMonth++;
        }
        long secondDate = (anotherDateTime.date.year * 365) + sum + secondLeap + anotherDateTime.date.day;
        long dayDiff = firstDate - secondDate;
        int firstTime = time.hour * 60 + time.minute;
        int secondTime = anotherDateTime.time.hour * 60 + anotherDateTime.time.minute;
        if (dayDiff < 0) {
            int minDiff = secondTime - firstTime;
            if (minDiff < 0) {
                dayDiff++;
                minDiff += 1440;
                return dayDiff * (-1) + " day(s) " + minDiff / 60 +" hour(s) " + minDiff % 60 + " minute(s)";
            } else {
                return dayDiff + " day(s) " + minDiff / 60 +" hour(s) " + minDiff % 60 + " minute(s)";
            }
        }else  if (dayDiff > 0){
            int minDiff = firstTime - secondTime;
            if (minDiff > 0) {
                return dayDiff + " day(s) " + minDiff / 60 +" hour(s) " + minDiff % 60 + " minute(s)";
            }
            else {
                dayDiff--;
                minDiff += 1440;
                return dayDiff + " day(s) " + minDiff / 60 +" hour(s) " + minDiff % 60 + " minute(s)";
            }
        } else {
            int minDiff = firstTime - secondTime;
            if (minDiff < 0)
                minDiff = minDiff * -1;
            return minDiff / 60 + " hour(s) " + minDiff % 60 + " minute(s)";
        }
    }
}
