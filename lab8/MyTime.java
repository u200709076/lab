public class MyTime {
    int hour;
    int minute;

    public MyTime(int hour , int minute) {
        this.hour = hour;
        this.minute = minute;
    }


    public String toString(){
        return (hour < 10 ? "0" : "") + hour + ":" + (minute< 10 ? "0" : "") + minute;
    }


    public int incrementHour(int diff) {
        int dayDiff = 0;

        if ((hour + diff) <0) {
            dayDiff = -1;
        }
        dayDiff += (hour + diff) / 24;
        hour = (hour + diff) % 24;
        if (hour < 0)
            hour += 24;
        return dayDiff;
    }

    public int incrementMinute(int diff) {
        int hourdiff = 0;
        if ((minute + diff) < 0){
            hourdiff = -1;
        }

        hourdiff += (minute + diff) / 60;
        minute = (minute + diff) % 60;

        if (minute <0)
            minute += 60;

        return incrementHour(hourdiff);
    }
    public  boolean isBefore (MyDateTime anotherDateTime) {
        if ((hour - anotherDateTime.time.hour) > 0) {
            return false;
        } else if ((minute - anotherDateTime.time.minute) > 0) {
            return false;
        } else
            return true;
    }
}
