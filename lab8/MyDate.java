import java.util.Date;

public class MyDate {

	int day , month ,year;

	int[] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};

	public MyDate(int day, int month, int year){
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public String toString(){
		return year +"-"+(month < 10 ? "0" :"") +month+"-"+(day < 10 ? "0":"") + day;
	}


	public void incrementDay() {
		day++;
		if (day > maxDays[month-1]){
			day = 1;
			month++;
		}
		if (month == 2 && day == 29 && !inLeapYear()){
			day = 1; month++ ;
		}
	}
	public boolean inLeapYear(){
		return ((year % 4) == 0);
	}

	public void incrementYear(int diff) {
		year += diff;
	}

	public void decrementDay() {
		day--;
		if (day == 0){
			month--;
			if (!inLeapYear() && month == 2){
				day = 28;
			} else {
				day = maxDays[month-1];
			}
		}
	}

	public void decrementYear() {
		year--;
		if (!inLeapYear() && month == 2)
			day = 28;
		if (inLeapYear() && month == 2)
			day = 29;
	}

	public void decrementMonth() {
		month--;
		if (month == 0 && day > maxDays[month]){
			year--;
			day = maxDays[month];
		} else if (month <= 0) {
			month = 12;
			year--;
		}
		while (day > maxDays[month-1]){
			day--;
		}
	}

	public void incrementDay(int diff) {
		while (diff > 0){
			incrementDay();
			diff--;
		}
	}

	public void decrementMonth(int diff) {
		while (diff > 0){
			decrementMonth();
			diff--;
		}
	}

	public void decrementDay(int diff) {
		while (diff > 0){
			decrementDay();
			diff--;
		}
	}

	public void incerementMonth(){
		month++;
		if (month == 13){
			month = 1;
			year++;
		}
		if (day > maxDays[month-1])
			day = maxDays[month-1];
	}

	public void incrementMonth(int diff) {
		while (diff > 0){
			incerementMonth();
			diff--;
		}
	}

	public void decrementYear(int diff) {
		while (diff > 0){
			decrementYear();
			diff--;
		}
	}

	public void incrementMonth() {
		month++;
		if (month == 13){
			month = 1;
			year++;
		}
		if (day > maxDays[month-1])
			day = maxDays[month-1];
	}

	public void incrementYear() {
		year++;
	}

	public boolean isBefore(MyDate anotherDate) {
		if ((year - anotherDate.year) > 0){
			return false;
		} else if (month - (anotherDate.month) > 0){
			return false;
		} else if (day - (anotherDate.day) > 0){
			return false;
		}
		else return true;
	}

	public boolean isAfter(MyDate anotherDate) {
		if ((year - anotherDate.year) > 0){
			return true;
		} else if (month - (anotherDate.month) > 0){
			return true;
		} else if (day - (anotherDate.day) > 0){
			return true;
		}
		else return false;
	}

	public int dayDifference(MyDate anotherDate) {
		int secondYear = anotherDate.year;
		int secondMonth = anotherDate.month;
		int secondDay = anotherDate.day;
		int firstLeap = year / 4;
		int secondLeap = secondYear / 4;
		int thMonth = 0;
		int sum = 0;
		while (thMonth < month-1){
			if (!inLeapYear() && thMonth == 1)
				maxDays[thMonth] = 28;
			sum += maxDays[thMonth];
			thMonth++;
		}
		long firstDate = year * 365 + sum + day + firstLeap;
		thMonth = 0;
		sum = 0;
		while (thMonth < secondMonth-1){
			if (!inLeapYear() && thMonth == 1)
				maxDays[thMonth] = 28;
			sum += maxDays[thMonth];
			thMonth++;
		}
		long secondDate = (secondYear * 365) + sum + secondDay + secondLeap;
		if (firstDate > secondDate)
			return (int) (firstDate - secondDate);
		else
			return (int) (secondDate - firstDate);
	}


}
