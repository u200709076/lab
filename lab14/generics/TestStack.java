package generics;

import java.util.List;

public class TestStack {

    public static void main(String[] args) {
        //testStack(new StackImpl());

        testStack(new StackArrayListImpl<Number>());


    }

    public static void testStack(Stack<Number> stack){

        stack.push(5);
        //stack.push("Hello");
        stack.push(6.5);
        stack.push(3000000000L);
        stack.push(11);
        stack.push(23);


        System.out.println("Stack1 : " +stack.toList());

        Stack<Integer> stack2 = new StackImpl<>();
        stack2.push(44);
        stack2.push(11);
        stack2.push(55);
        stack2.push(33);
        System.out.println("Stack2 : " +stack2.toList());

        stack.addAll(stack2);
        System.out.println("Stack1" +stack.toList());


        while (!stack.empty()){
            System.out.println(stack.pop());
        }
        System.out.println(stack.toList());
    }
}
