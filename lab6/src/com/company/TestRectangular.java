package com.company;

public class TestRectangular {
    public static void main(String[] args) {

        Point p = new Point(3,2);
        Rectangular r = new Rectangular(p,10 ,12);
        System.out.println("Area of rectangular is "+ r.area());

        System.out.println("Perimeter of rectangular is "+ r.perimeter());

        Point[] corners = r.corners();

        for (int i = 0 ; i < corners.length; i++){
            p = corners[i];
            if (p == null)
                System.out.println("p is null");
            else {
                System.out.println("Corner " + (i+1) + " x = "+ p.getxCoord()
                        + " y " + p.getyCoord());

            }
        }
    }
}
