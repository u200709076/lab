package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) throws InvalidMoveException{
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();

            boolean invalidRow=false;

            int row=0;
            int col=0;

            do {
                System.out.print("Player " + player + " enter row number:");
                try {
                    row = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid integer");
                    invalidRow=false;
                }
                catch (NumberFormatException ex) {
                    invalidRow=true;
                    System.out.println("Invalid integer");

                }
            }while(invalidRow);

            boolean invalidColumn=false;
            do{
                System.out.print("Player " + player + " enter column number:");
                try {
                    col = Integer.valueOf(reader.nextLine());
                    System.out.println("valid integer");
                    invalidColumn = false;
                }catch (NumberFormatException ex) {
                    invalidColumn=true;
                    System.out.println("Invalid integer");

                }

            }while(invalidColumn);


            try {
                board.move(row, col);

            }catch(InvalidMoveException ex){
                System.out.println(ex.getMessage());
            }

            System.out.println(board);
        }


        reader.close();
    }


}