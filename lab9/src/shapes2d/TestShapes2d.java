package shapes2d;

public class TestShapes2d {

    public static void main(String[] args) {

        Circle c1 = new Circle(5);
        c1 = new Circle(6);
        Circle c2 = new Circle(6);
        String str = new String("Hello");      // object class is immutable
        String str2 = " Hola";

        //str == str2   compares objects not content
        System.out.println(str.equals(str2));        // correct comparison of strings


        System.out.println("c1 == c2 : " + (c1 == c2));

        System.out.println("c1 == c2 : " + (c1.equals(c2)));

    }
}
