package shapes2d;

public class Square {

    protected int side;

    public int area(){
        return side * side;
    }

    public Square(int side) {
        this.side = side;
    }

    @Override
    public String toString(){
        return "Side = " + side;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj.getClass().equals(this.getClass())) {
            Square c = (Square) obj;
            return side == c.side;
        }
        return false;
    }
}
